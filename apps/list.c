#include <stdio.h>
#include <stdlib.h>
#include "list.h"

void addNode(struct node **list, int v) {
  struct node* new = (struct node*)malloc(sizeof(struct node));
  new->next = NULL;
  new->value = v;

  if (*list == NULL) {
    *list = new;
    return;
  }

  struct node *actual = *list;
  while (actual->next != NULL) {
    actual = actual->next;
  }

  actual->next = new;
}

int removeNode(struct node **list, int pos) {
  if (pos == 0) {
    struct node *rem = *list;
    *list = (*list)->next;
    int v = rem->value;
    free(rem);
    return v;
  }

  struct node *actual = *list;
  for (int i = 0; i < pos-1; i++) {
    actual = actual->next;
  }

  struct node *rem = actual->next;
  actual->next = rem->next;
  int v = rem->value;
  free(rem);
  return v;
}

int getNode(struct node *list, int pos) {
  struct node *actual = list;
  for (int i = 0; i < pos; i++) {
    actual = actual->next;
  }

  return actual->value;
}

void setNode(struct node **list, int pos, int val) {
  struct node *actual = *list;
  for (int i = 0; i < pos; i++) {
    actual = actual->next;
  }

   actual->value = val;
}