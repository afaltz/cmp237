	.file	1 "listexample.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.abicalls
	.rdata
	.align	2
$LC0:
	.ascii	"%d \000"
	.text
	.align	2
	.globl	printList
	.set	mips16
	.set	nomicromips
	.ent	printList
	.type	printList, @function
printList:
	.frame	$17,32,$31		# vars= 8, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	addiu	$sp,-48
	move	$3,$31
	sw	$3,44($sp)
	sw	$17,40($sp)
	sw	$16,36($sp)
	addiu	$17,$sp,16
	lw	$2,$L4
	move	$28,$2
	sw	$2,0($17)
	move	$16,$28
	sw	$4,32($17)
	lw	$2,32($17)
	sw	$2,12($17)
	b	$L2
$L3:
	lw	$2,12($17)
	lw	$2,4($2)
	move	$5,$2
	lw	$4,$L5
	lw	$2,%call16(printf)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,12($17)
	lw	$2,0($2)
	sw	$2,12($17)
$L2:
	lw	$2,12($17)
	bnez	$2,$L3
	li	$4,10
	lw	$2,%call16(putchar)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	.set	noreorder
	nop
	.set	reorder
	move	$sp,$17
	lw	$7,28($sp)
	lw	$17,24($sp)
	lw	$16,20($sp)
	addiu	$sp,32
	j	$7
	.align	2
$L4:
	.word	__gnu_local_gp
$L5:
	.word	$LC0
	.end	printList
	.size	printList, .-printList
	.rdata
	.align	2
$LC3:
	.ascii	"Second element: %d\012\012\000"
	.align	2
$LC4:
	.ascii	"Removing second element: %d\012\000"
	.align	2
$LC5:
	.ascii	"Removing first element: %d\012\012\000"
	.align	2
$LC6:
	.ascii	"Second element: %d\012\000"
	.text
	.align	2
	.globl	main
	.set	mips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$17,32,$31		# vars= 8, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	addiu	$sp,-48
	move	$3,$31
	sw	$3,44($sp)
	sw	$17,40($sp)
	sw	$16,36($sp)
	addiu	$17,$sp,16
	lw	$2,$L9
	move	$28,$2
	sw	$2,0($17)
	move	$16,$28
	lw	$2,%got(__stack_chk_guard)($16)
	lw	$2,0($2)
	sw	$2,12($17)
	li	$2,0
	sw	$2,8($17)
	addiu	$2,$17,7
	addiu	$2,1
	li	$5,0
	move	$4,$2
	lw	$2,%call16(addNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	addiu	$2,$17,7
	addiu	$2,1
	li	$5,1
	move	$4,$2
	lw	$2,%call16(addNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	addiu	$2,$17,7
	addiu	$2,1
	li	$5,10
	move	$4,$2
	lw	$2,%call16(addNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	addiu	$2,$17,7
	addiu	$2,1
	li	$5,100
	move	$4,$2
	lw	$2,%call16(addNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	li	$3,1000
	addiu	$2,$17,7
	addiu	$2,1
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(addNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,8($17)
	move	$4,$2
	.option	pic0
	jal	printList
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,8($17)
	li	$5,1
	move	$4,$2
	lw	$2,%call16(getNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	move	$5,$2
	lw	$4,$L10
	lw	$2,%call16(printf)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	addiu	$2,$17,7
	addiu	$2,1
	li	$5,1
	move	$4,$2
	lw	$2,%call16(removeNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	move	$5,$2
	lw	$4,$L11
	lw	$2,%call16(printf)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	addiu	$2,$17,7
	addiu	$2,1
	li	$5,0
	move	$4,$2
	lw	$2,%call16(removeNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	move	$5,$2
	lw	$4,$L12
	lw	$2,%call16(printf)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,8($17)
	move	$4,$2
	.option	pic0
	jal	printList
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,8($17)
	li	$5,1
	move	$4,$2
	lw	$2,%call16(getNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	move	$5,$2
	lw	$4,$L13
	lw	$2,%call16(printf)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	li	$2,0
	lw	$3,%got(__stack_chk_guard)($16)
	lw	$4,12($17)
	lw	$3,0($3)
	xor	$3,$4
	beqz	$3,$L8
	lw	$2,%call16(__stack_chk_fail)($16)
	move	$25,$2
	jalr	$2
$L8:
	move	$sp,$17
	lw	$7,28($sp)
	lw	$17,24($sp)
	lw	$16,20($sp)
	addiu	$sp,32
	j	$7
	.align	2
$L9:
	.word	__gnu_local_gp
$L10:
	.word	$LC3
$L11:
	.word	$LC4
$L12:
	.word	$LC5
$L13:
	.word	$LC6
	.end	main
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609"
