	.file	1 "string.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.abicalls
	.text
	.align	2
	.globl	createString
	.set	mips16
	.set	nomicromips
	.ent	createString
	.type	createString, @function
createString:
	.frame	$17,32,$31		# vars= 8, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	addiu	$sp,-48
	move	$3,$31
	sw	$3,44($sp)
	sw	$17,40($sp)
	sw	$16,36($sp)
	addiu	$17,$sp,16
	lw	$2,$L4
	move	$28,$2
	sw	$2,0($17)
	move	$16,$28
	sw	$4,32($17)
	sw	$5,36($17)
	sw	$6,40($17)
	li	$2,0
	sw	$2,12($17)
	b	$L2
$L3:
	lw	$2,12($17)
	lw	$3,36($17)
	addu	$2,$3,$2
	lb	$2,0($2)
	move	$3,$2
	lw	$2,32($17)
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(addNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,12($17)
	addiu	$2,1
	sw	$2,12($17)
$L2:
	lw	$3,12($17)
	lw	$2,40($17)
	slt	$3,$2
	move	$2,$24
	bnez	$2,$L3
	.set	noreorder
	nop
	.set	reorder
	move	$sp,$17
	lw	$7,28($sp)
	lw	$17,24($sp)
	lw	$16,20($sp)
	addiu	$sp,32
	j	$7
	.align	2
$L4:
	.word	__gnu_local_gp
	.end	createString
	.size	createString, .-createString
	.align	2
	.globl	substring
	.set	mips16
	.set	nomicromips
	.ent	substring
	.type	substring, @function
substring:
	.frame	$17,24,$31		# vars= 16, regs= 1/0, args= 0, gp= 0
	.mask	0x00020000,-4
	.fmask	0x00000000,0
	addiu	$sp,-24
	sw	$17,20($sp)
	move	$17,$sp
	sw	$4,24($17)
	sw	$5,28($17)
	li	$2,0
	sw	$2,0($17)
	lw	$2,24($17)
	sw	$2,4($17)
	b	$L6
$L13:
	lw	$2,4($17)
	sw	$2,8($17)
	lw	$2,28($17)
	sw	$2,12($17)
	b	$L7
$L11:
	lw	$2,8($17)
	lw	$3,4($2)
	lw	$2,12($17)
	lw	$2,4($2)
	xor	$2,$3
	bnez	$2,$L15
	lw	$2,8($17)
	lw	$2,0($2)
	sw	$2,8($17)
	lw	$2,12($17)
	lw	$2,0($2)
	bnez	$2,$L10
	li	$2,1
	sw	$2,0($17)
$L10:
	lw	$2,12($17)
	lw	$2,0($2)
	sw	$2,12($17)
$L7:
	lw	$2,12($17)
	bnez	$2,$L11
	b	$L9
$L15:
	.set	noreorder
	nop
	.set	reorder
$L9:
	lw	$2,4($17)
	lw	$2,0($2)
	sw	$2,4($17)
$L6:
	lw	$2,4($17)
	beqz	$2,$L12
	lw	$2,0($17)
	beqz	$2,$L13
$L12:
	lw	$2,0($17)
	move	$sp,$17
	lw	$17,20($sp)
	addiu	$sp,24
	j	$31
	.end	substring
	.size	substring, .-substring
	.rdata
	.align	2
$LC1:
	.ascii	"Lorem Ipsum\000"
	.align	2
$LC2:
	.ascii	"Ipsum\000"
	.align	2
$LC3:
	.ascii	"Vish\000"
	.align	2
$LC4:
	.ascii	"s2 exists in s1? %d\012\000"
	.align	2
$LC5:
	.ascii	"s3 exists in s1? %d\012\000"
	.text
	.align	2
	.globl	main
	.set	mips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$17,40,$31		# vars= 16, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	addiu	$sp,-56
	move	$3,$31
	sw	$3,52($sp)
	sw	$17,48($sp)
	sw	$16,44($sp)
	addiu	$17,$sp,16
	lw	$2,$L19
	move	$28,$2
	sw	$2,0($17)
	move	$16,$28
	lw	$2,%got(__stack_chk_guard)($16)
	lw	$2,0($2)
	sw	$2,20($17)
	li	$2,0
	sw	$2,8($17)
	li	$2,0
	sw	$2,12($17)
	li	$2,0
	sw	$2,16($17)
	addiu	$2,$17,7
	addiu	$2,1
	li	$6,11
	lw	$5,$L20
	move	$4,$2
	.option	pic0
	jal	createString
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
	addiu	$2,$17,7
	addiu	$2,5
	li	$6,5
	lw	$5,$L21
	move	$4,$2
	.option	pic0
	jal	createString
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
	addiu	$2,$17,7
	addiu	$2,9
	li	$6,4
	lw	$5,$L22
	move	$4,$2
	.option	pic0
	jal	createString
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,8($17)
	lw	$3,12($17)
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	substring
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
	move	$5,$2
	lw	$4,$L23
	lw	$2,%call16(printf)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,8($17)
	lw	$3,16($17)
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	substring
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
	move	$5,$2
	lw	$4,$L24
	lw	$2,%call16(printf)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	li	$2,0
	lw	$3,%got(__stack_chk_guard)($16)
	lw	$4,20($17)
	lw	$3,0($3)
	xor	$3,$4
	beqz	$3,$L18
	lw	$2,%call16(__stack_chk_fail)($16)
	move	$25,$2
	jalr	$2
$L18:
	move	$sp,$17
	lw	$7,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	addiu	$sp,40
	j	$7
	.align	2
$L19:
	.word	__gnu_local_gp
$L20:
	.word	$LC1
$L21:
	.word	$LC2
$L22:
	.word	$LC3
$L23:
	.word	$LC4
$L24:
	.word	$LC5
	.end	main
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609"
