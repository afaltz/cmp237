	.file	1 "list.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.abicalls
	.text
	.align	2
	.globl	addNode
	.set	mips16
	.set	nomicromips
	.ent	addNode
	.type	addNode, @function
addNode:
	.frame	$17,24,$31		# vars= 8, regs= 2/0, args= 16, gp= 8
	.mask	0x80020000,-4
	.fmask	0x00000000,0
	addiu	$sp,-40
	move	$3,$31
	sw	$3,36($sp)
	sw	$17,32($sp)
	addiu	$17,$sp,16
	lw	$2,$L6
	move	$28,$2
	sw	$2,0($17)
	move	$2,$28
	sw	$4,24($17)
	sw	$5,28($17)
	li	$4,8
	lw	$2,%call16(malloc)($2)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	sw	$2,12($17)
	lw	$2,12($17)
	li	$3,0
	sw	$3,0($2)
	lw	$2,12($17)
	lw	$3,28($17)
	sw	$3,4($2)
	lw	$2,24($17)
	lw	$2,0($2)
	bnez	$2,$L2
	lw	$2,24($17)
	lw	$3,12($17)
	sw	$3,0($2)
	b	$L1
$L2:
	lw	$2,24($17)
	lw	$2,0($2)
	sw	$2,8($17)
	b	$L4
$L5:
	lw	$2,8($17)
	lw	$2,0($2)
	sw	$2,8($17)
$L4:
	lw	$2,8($17)
	lw	$2,0($2)
	bnez	$2,$L5
	lw	$2,8($17)
	lw	$3,12($17)
	sw	$3,0($2)
$L1:
	move	$sp,$17
	lw	$7,20($sp)
	lw	$17,16($sp)
	addiu	$sp,24
	j	$7
	.align	2
$L6:
	.word	__gnu_local_gp
	.end	addNode
	.size	addNode, .-addNode
	.align	2
	.globl	removeNode
	.set	mips16
	.set	nomicromips
	.ent	removeNode
	.type	removeNode, @function
removeNode:
	.frame	$17,40,$31		# vars= 24, regs= 2/0, args= 16, gp= 8
	.mask	0x80020000,-4
	.fmask	0x00000000,0
	addiu	$sp,-56
	move	$3,$31
	sw	$3,52($sp)
	sw	$17,48($sp)
	addiu	$17,$sp,16
	lw	$2,$L12
	move	$28,$2
	sw	$2,0($17)
	move	$2,$28
	sw	$4,40($17)
	sw	$5,44($17)
	lw	$3,44($17)
	bnez	$3,$L8
	lw	$3,40($17)
	lw	$3,0($3)
	sw	$3,16($17)
	lw	$3,40($17)
	lw	$3,0($3)
	lw	$4,0($3)
	lw	$3,40($17)
	sw	$4,0($3)
	lw	$3,16($17)
	lw	$3,4($3)
	sw	$3,20($17)
	lw	$3,16($17)
	move	$4,$3
	lw	$2,%call16(free)($2)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,20($17)
	b	$L9
$L8:
	lw	$3,40($17)
	lw	$3,0($3)
	sw	$3,8($17)
	li	$3,0
	sw	$3,12($17)
	b	$L10
$L11:
	lw	$3,8($17)
	lw	$3,0($3)
	sw	$3,8($17)
	lw	$3,12($17)
	addiu	$3,1
	sw	$3,12($17)
$L10:
	lw	$3,44($17)
	addiu	$4,$3,-1
	lw	$3,12($17)
	slt	$3,$4
	move	$3,$24
	bnez	$3,$L11
	lw	$3,8($17)
	lw	$3,0($3)
	sw	$3,24($17)
	lw	$3,24($17)
	lw	$4,0($3)
	lw	$3,8($17)
	sw	$4,0($3)
	lw	$3,24($17)
	lw	$3,4($3)
	sw	$3,28($17)
	lw	$3,24($17)
	move	$4,$3
	lw	$2,%call16(free)($2)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,28($17)
$L9:
	move	$sp,$17
	lw	$7,36($sp)
	lw	$17,32($sp)
	addiu	$sp,40
	j	$7
	.align	2
$L12:
	.word	__gnu_local_gp
	.end	removeNode
	.size	removeNode, .-removeNode
	.align	2
	.globl	getNode
	.set	mips16
	.set	nomicromips
	.ent	getNode
	.type	getNode, @function
getNode:
	.frame	$17,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x00020000,-4
	.fmask	0x00000000,0
	addiu	$sp,-16
	sw	$17,12($sp)
	move	$17,$sp
	sw	$4,16($17)
	sw	$5,20($17)
	lw	$2,16($17)
	sw	$2,0($17)
	li	$2,0
	sw	$2,4($17)
	b	$L14
$L15:
	lw	$2,0($17)
	lw	$2,0($2)
	sw	$2,0($17)
	lw	$2,4($17)
	addiu	$2,1
	sw	$2,4($17)
$L14:
	lw	$3,4($17)
	lw	$2,20($17)
	slt	$3,$2
	move	$2,$24
	bnez	$2,$L15
	lw	$2,0($17)
	lw	$2,4($2)
	move	$sp,$17
	lw	$17,12($sp)
	addiu	$sp,16
	j	$31
	.end	getNode
	.size	getNode, .-getNode
	.align	2
	.globl	setNode
	.set	mips16
	.set	nomicromips
	.ent	setNode
	.type	setNode, @function
setNode:
	.frame	$17,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x00020000,-4
	.fmask	0x00000000,0
	addiu	$sp,-16
	sw	$17,12($sp)
	move	$17,$sp
	sw	$4,16($17)
	sw	$5,20($17)
	sw	$6,24($17)
	lw	$2,16($17)
	lw	$2,0($2)
	sw	$2,0($17)
	li	$2,0
	sw	$2,4($17)
	b	$L18
$L19:
	lw	$2,0($17)
	lw	$2,0($2)
	sw	$2,0($17)
	lw	$2,4($17)
	addiu	$2,1
	sw	$2,4($17)
$L18:
	lw	$3,4($17)
	lw	$2,20($17)
	slt	$3,$2
	move	$2,$24
	bnez	$2,$L19
	lw	$2,0($17)
	lw	$3,24($17)
	sw	$3,4($2)
	.set	noreorder
	nop
	.set	reorder
	move	$sp,$17
	lw	$17,12($sp)
	addiu	$sp,16
	j	$31
	.end	setNode
	.size	setNode, .-setNode
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609"
