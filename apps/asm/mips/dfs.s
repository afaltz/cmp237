	.file	1 "dfs.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.abicalls
	.rdata
	.align	2
$LC0:
	.word	0
	.word	1
	.word	1
	.word	1
	.word	1
	.word	0
	.word	0
	.word	0
	.word	1
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	0
	.word	0
	.word	1
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	0
	.word	0
	.word	1
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	0
	.word	1
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	0
	.word	0
	.word	1
	.word	1
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	0
	.word	0
	.word	0
	.word	1
	.word	1
	.word	0
	.word	0
	.word	1
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	1
	.word	0
	.text
	.align	2
	.globl	createGraph
	.set	mips16
	.set	nomicromips
	.ent	createGraph
	.type	createGraph, @function
createGraph:
	.frame	$17,304,$31		# vars= 280, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	addiu	$sp,-320
	move	$3,$31
	sw	$3,316($sp)
	sw	$17,312($sp)
	sw	$16,308($sp)
	addiu	$17,$sp,16
	lw	$2,$L9
	move	$28,$2
	sw	$2,0($17)
	move	$16,$28
	sw	$4,12($17)
	sw	$5,8($17)
	lw	$2,%got(__stack_chk_guard)($16)
	lw	$2,0($2)
	sw	$2,284($17)
	lw	$3,$L10
	addiu	$2,$17,7
	addiu	$2,21
	move	$4,$3
	li	$3,255
	addiu	$3,1
	move	$6,$3
	move	$5,$4
	move	$4,$2
	lw	$2,%call16(memcpy)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	li	$2,0
	sw	$2,16($17)
	b	$L2
$L5:
	li	$2,0
	sw	$2,20($17)
	b	$L3
$L4:
	lw	$2,16($17)
	sll	$3,$2,3
	lw	$2,20($17)
	addu	$2,$3,$2
	sll	$2,$2,2
	addiu	$3,$17,288
	addu	$2,$3,$2
	lw	$3,-260($2)
	lw	$2,12($17)
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(addNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,20($17)
	addiu	$2,1
	sw	$2,20($17)
$L3:
	lw	$2,20($17)
	slt	$2,8
	move	$2,$24
	bnez	$2,$L4
	lw	$2,16($17)
	addiu	$2,1
	sw	$2,16($17)
$L2:
	lw	$2,16($17)
	slt	$2,8
	move	$2,$24
	bnez	$2,$L5
	li	$2,0
	sw	$2,24($17)
	b	$L6
$L7:
	lw	$2,8($17)
	li	$5,0
	move	$4,$2
	lw	$2,%call16(addNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$2,24($17)
	addiu	$2,1
	sw	$2,24($17)
$L6:
	lw	$2,24($17)
	slt	$2,8
	move	$2,$24
	bnez	$2,$L7
	.set	noreorder
	nop
	.set	reorder
	lw	$2,%got(__stack_chk_guard)($16)
	lw	$3,284($17)
	lw	$2,0($2)
	xor	$2,$3
	beqz	$2,$L8
	lw	$2,%call16(__stack_chk_fail)($16)
	move	$25,$2
	jalr	$2
$L8:
	move	$sp,$17
	lw	$7,300($sp)
	lw	$17,296($sp)
	lw	$16,292($sp)
	addiu	$sp,304
	j	$7
	.align	2
$L9:
	.word	__gnu_local_gp
$L10:
	.word	$LC0
	.end	createGraph
	.size	createGraph, .-createGraph
	.rdata
	.align	2
$LC3:
	.ascii	"Visited: %d\012\000"
	.text
	.align	2
	.globl	DFS
	.set	mips16
	.set	nomicromips
	.ent	DFS
	.type	DFS, @function
DFS:
	.frame	$17,32,$31		# vars= 8, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	addiu	$sp,-48
	move	$3,$31
	sw	$3,44($sp)
	sw	$17,40($sp)
	sw	$16,36($sp)
	addiu	$17,$sp,16
	lw	$2,$L15
	move	$28,$2
	sw	$2,0($17)
	move	$16,$28
	sw	$4,32($17)
	sw	$5,36($17)
	sw	$6,40($17)
	sw	$7,44($17)
	lw	$2,44($17)
	move	$5,$2
	lw	$4,$L16
	lw	$2,%call16(printf)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	lw	$3,44($17)
	lw	$2,40($17)
	li	$6,1
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(setNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	li	$2,0
	sw	$2,12($17)
	b	$L12
$L14:
	lw	$2,40($17)
	lw	$2,0($2)
	lw	$3,12($17)
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(getNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	bnez	$2,$L13
	lw	$3,44($17)
	lw	$2,32($17)
	mult	$3,$2
	mflo	$3
	lw	$2,12($17)
	addu	$3,$3,$2
	lw	$2,36($17)
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(getNode)($16)
	move	$25,$2
	jalr	$2
	lw	$6,0($17)
	move	$28,$6
	li	$3,1
	xor	$2,$3
	bnez	$2,$L13
	lw	$5,12($17)
	lw	$4,40($17)
	lw	$3,36($17)
	lw	$2,32($17)
	move	$7,$5
	move	$6,$4
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	DFS
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
$L13:
	lw	$2,12($17)
	addiu	$2,1
	sw	$2,12($17)
$L12:
	lw	$3,12($17)
	lw	$2,32($17)
	slt	$3,$2
	move	$2,$24
	bnez	$2,$L14
	.set	noreorder
	nop
	.set	reorder
	move	$sp,$17
	lw	$7,28($sp)
	lw	$17,24($sp)
	lw	$16,20($sp)
	addiu	$sp,32
	j	$7
	.align	2
$L15:
	.word	__gnu_local_gp
$L16:
	.word	$LC3
	.end	DFS
	.size	DFS, .-DFS
	.align	2
	.globl	main
	.set	mips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$17,40,$31		# vars= 16, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	addiu	$sp,-56
	move	$3,$31
	sw	$3,52($sp)
	sw	$17,48($sp)
	sw	$16,44($sp)
	addiu	$17,$sp,16
	lw	$2,$L19
	move	$28,$2
	sw	$2,0($17)
	move	$16,$28
	lw	$2,%got(__stack_chk_guard)($16)
	lw	$2,0($2)
	sw	$2,20($17)
	li	$2,0
	sw	$2,8($17)
	li	$2,0
	sw	$2,12($17)
	li	$2,8
	sw	$2,16($17)
	addiu	$3,$17,7
	addiu	$3,5
	addiu	$2,$17,7
	addiu	$2,1
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	createGraph
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
	lw	$3,8($17)
	addiu	$4,$17,7
	addiu	$4,5
	lw	$2,16($17)
	li	$7,0
	move	$6,$4
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	DFS
	.option	pic2
	lw	$6,0($17)
	move	$28,$6
	.set	noreorder
	nop
	.set	reorder
	lw	$2,%got(__stack_chk_guard)($16)
	lw	$3,20($17)
	lw	$2,0($2)
	xor	$2,$3
	beqz	$2,$L18
	lw	$2,%call16(__stack_chk_fail)($16)
	move	$25,$2
	jalr	$2
$L18:
	move	$sp,$17
	lw	$7,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	addiu	$sp,40
	j	$7
	.align	2
$L19:
	.word	__gnu_local_gp
	.end	main
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609"
