#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef LIST_H_
#define LIST_H_

struct node {
  struct node *next;
  int value;
};

void addNode(struct node **list, int v);
int removeNode(struct node **list, int pos);
int getNode(struct node *list, int pos);
void setNode(struct node **list, int pos, int val);

#endif