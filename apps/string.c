#include <stdio.h>
#include <stdlib.h>
#include "list.h"

void createString(struct node **s, char str[], int str_size) {
  for (int i = 0; i < str_size; i++) {
    addNode(s, str[i]);
  }
}

int substring(struct node *s1, struct node *s2) {
  int r = FALSE;

  for (struct node *actual = s1; actual != NULL && !r; actual = actual->next) {
    struct node *actual1 = actual;

    for (struct node *actual2 = s2; actual2 != NULL; actual2 = actual2->next) {
      if (actual1->value != actual2->value) {
        break;
      } else {
        actual1 = actual1->next;

        if (actual2->next == NULL) {
          r = TRUE;
        }
      }
    }
  }

  return r;
}

int main() {
  struct node *str1 = NULL;
  struct node *str2 = NULL;
  struct node *str3 = NULL;

  createString(&str1, "Lorem Ipsum", 11);
  createString(&str2, "Ipsum", 5);
  createString(&str3, "Vish", 4);
  printf("s2 exists in s1? %d\n", substring(str1, str2));
  printf("s3 exists in s1? %d\n", substring(str1, str3));
  return 0;
}
