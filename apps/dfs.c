#include <stdio.h>
#include "list.h"

void createGraph(struct node **graph, struct node **visited) {
  int graphArr[8][8] = {
    {0, 1, 1, 1, 1, 0, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 1, 0, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {1, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 1, 0, 0, 0, 0, 1},
    {0, 0, 0, 1, 1, 0, 0, 1},
    {0, 0, 0, 0, 0, 1, 1, 0},
  };

  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      addNode(graph, graphArr[i][j]);
    }
  }

  for (int i = 0; i < 8; i++) {
    addNode(visited, FALSE);
  }
}

void DFS(int graph_size, struct node *graph, struct node **visited, int actual) {
  printf("Visited: %d\n", actual);
  setNode(visited, actual, TRUE);

  for (int i = 0; i < graph_size; i++) {
    if (!getNode(*visited, i) && getNode(graph, (actual * graph_size) + i) == 1) {
      DFS(graph_size, graph, visited, i);
    }
  }
}

void main() {
  struct node *graph = NULL;
  struct node *visited = NULL;
  int size = 8;
  
  createGraph(&graph, &visited);
  DFS(size, graph, &visited, 0);
}
