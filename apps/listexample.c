#include <stdio.h>
#include <stdlib.h>
#include "list.h"

void printList(struct node *list) {
  struct node *actual = list;

  while(actual != NULL) {
    printf("%d ", actual->value);
    actual = actual->next;
  }

  printf("\n");
}

int main() {
  struct node *list = NULL;

  addNode(&list, 0);
  addNode(&list, 1);
  addNode(&list, 10);
  addNode(&list, 100);
  addNode(&list, 1000);

  printList(list);
  printf("Second element: %d\n\n", getNode(list, 1));

  printf("Removing second element: %d\n", removeNode(&list, 1));
  printf("Removing first element: %d\n\n", removeNode(&list, 0));

  printList(list);
  printf("Second element: %d\n", getNode(list, 1));
  return 0;
}